PGDMP     +                	    x            banca    11.8    12.4 N    j           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            k           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            l           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            m           1262    16393    banca    DATABASE     �   CREATE DATABASE banca WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_United States.1252' LC_CTYPE = 'Spanish_United States.1252';
    DROP DATABASE banca;
                postgres    false            n           0    0    DATABASE banca    ACL     �   REVOKE CONNECT,TEMPORARY ON DATABASE banca FROM PUBLIC;
GRANT CREATE ON DATABASE banca TO administradosistema;
GRANT CREATE ON DATABASE banca TO dba;
                   postgres    false    2925            r           1247    16481    cuentaprecio    DOMAIN     k   CREATE DOMAIN public.cuentaprecio AS numeric(7,2)
	CONSTRAINT cuentaprecio_check CHECK ((VALUE >= 100.0));
 !   DROP DOMAIN public.cuentaprecio;
       public          postgres    false            w           1247    16959    precio    DOMAIN     _   CREATE DOMAIN public.precio AS numeric(5,2)
	CONSTRAINT valor CHECK ((VALUE >= (0)::numeric));
    DROP DOMAIN public.precio;
       public          postgres    false            n           1247    16467    sexo    DOMAIN     z   CREATE DOMAIN public.sexo AS character(1)
	CONSTRAINT sexo_check CHECK ((VALUE = ANY (ARRAY['M'::bpchar, 'F'::bpchar])));
    DROP DOMAIN public.sexo;
       public          postgres    false            |           1247    16977    tipo    DOMAIN     N   CREATE DOMAIN public.tipo AS integer
	CONSTRAINT regla1 CHECK ((VALUE = 10));
    DROP DOMAIN public.tipo;
       public          postgres    false            �            1255    17057    act_saldo(text, numeric)    FUNCTION     �   CREATE FUNCTION public.act_saldo(text, numeric) RETURNS integer
    LANGUAGE sql
    AS $_$update CUENTAS
set cueSaldo = cueSaldo + $2
where cuenumero = $1;
select 1;$_$;
 /   DROP FUNCTION public.act_saldo(text, numeric);
       public          postgres    false            o           0    0 !   FUNCTION act_saldo(text, numeric)    ACL     C   GRANT ALL ON FUNCTION public.act_saldo(text, numeric) TO operador;
          public          postgres    false    210            �            1255    16957    one()    FUNCTION     ]   CREATE FUNCTION public.one() RETURNS integer
    LANGUAGE sql
    AS $$select 1 as result$$;
    DROP FUNCTION public.one();
       public          postgres    false            p           0    0    FUNCTION one()    ACL     0   GRANT ALL ON FUNCTION public.one() TO operador;
          public          postgres    false    208            �            1255    17100    prueba()    FUNCTION     ^  CREATE FUNCTION public.prueba() RETURNS trigger
    LANGUAGE plpgsql
    AS $$Declare
Usuario varchar(250) := user;
Fecha date := current_date;
Tiempo Time := current_Time;

begin 

insert into "reg_clientes" values (2,old.clinombre,old.clicalle,old.cliciudad,old.clisexo,old.clifechanacimiento,old.clitipo,Fecha,Usuario,Tiempo);
return new;
End
$$;
    DROP FUNCTION public.prueba();
       public          postgres    false            q           0    0    FUNCTION prueba()    ACL     3   GRANT ALL ON FUNCTION public.prueba() TO operador;
          public          postgres    false    223            �            1255    17090 	   prueba2()    FUNCTION     �   CREATE FUNCTION public.prueba2() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
Declare
Usuario varchar(250) := user;
Fecha date := current_date;
Tiempo Time := current_Time;


begin 

insert into "long" values (Usuario,Fecha,Tiempo);
return new;
End
$$;
     DROP FUNCTION public.prueba2();
       public          postgres    false            r           0    0    FUNCTION prueba2()    ACL     4   GRANT ALL ON FUNCTION public.prueba2() TO operador;
          public          postgres    false    209            �            1259    16394    clientes    TABLE     0  CREATE TABLE public.clientes (
    clinombre character(30) NOT NULL,
    clicalle character(30) NOT NULL,
    cliciudad character(30) NOT NULL,
    clisexo public.sexo NOT NULL,
    clifechanacimiento date,
    clitipo character(3) NOT NULL,
    CONSTRAINT value CHECK ((clifechanacimiento <= now()))
);
    DROP TABLE public.clientes;
       public            postgres    false    622            s           0    0    TABLE clientes    ACL     �   GRANT SELECT ON TABLE public.clientes TO gerente;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.clientes TO jefe;
GRANT SELECT,UPDATE ON TABLE public.clientes TO cajero;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.clientes TO programador;
          public          postgres    false    196            �            1259    16400    cuentas    TABLE     �   CREATE TABLE public.cuentas (
    cuenumero character(7) NOT NULL,
    sucnombre character(30) NOT NULL,
    cuesaldo public.cuentaprecio NOT NULL,
    cuetipo character(5) NOT NULL,
    cuefecha date NOT NULL
);
    DROP TABLE public.cuentas;
       public            postgres    false    626            t           0    0    TABLE cuentas    ACL     �   GRANT SELECT ON TABLE public.cuentas TO gerente;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.cuentas TO jefe;
GRANT SELECT,UPDATE ON TABLE public.cuentas TO cajero;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.cuentas TO programador;
          public          postgres    false    197            �            1259    16407    debe    TABLE     h   CREATE TABLE public.debe (
    prenumero character(5) NOT NULL,
    clinombre character(30) NOT NULL
);
    DROP TABLE public.debe;
       public            postgres    false            �            1259    17104    empleados_insert    TABLE     �   CREATE TABLE public.empleados_insert (
    reg_id integer NOT NULL,
    nombreempleado character(50) NOT NULL,
    fecha date,
    users character(30),
    hora time without time zone
);
 $   DROP TABLE public.empleados_insert;
       public            postgres    false            �            1259    17102    empleados_insert_reg_id_seq    SEQUENCE     �   CREATE SEQUENCE public.empleados_insert_reg_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.empleados_insert_reg_id_seq;
       public          postgres    false    207            u           0    0    empleados_insert_reg_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.empleados_insert_reg_id_seq OWNED BY public.empleados_insert.reg_id;
          public          postgres    false    206            �            1259    17081    long    TABLE     b   CREATE TABLE public.long (
    usuario character(50),
    fecha date,
    tiempo character(50)
);
    DROP TABLE public.long;
       public            postgres    false            �            1259    16415 	   prestamos    TABLE     �   CREATE TABLE public.prestamos (
    prenumero character(5) NOT NULL,
    sucnombre character(30) NOT NULL,
    preimporte public.precio NOT NULL,
    prefecha date NOT NULL,
    fechavencimiento date
);
    DROP TABLE public.prestamos;
       public            postgres    false    631            v           0    0    TABLE prestamos    ACL     �   GRANT SELECT ON TABLE public.prestamos TO gerente;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.prestamos TO jefe;
GRANT SELECT,UPDATE ON TABLE public.prestamos TO cajero;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.prestamos TO programador;
          public          postgres    false    199            �            1259    17094    reg_clientes    TABLE     1  CREATE TABLE public.reg_clientes (
    reg_id integer NOT NULL,
    nombrecliente character(50) NOT NULL,
    calle character(50),
    ciudad character(30),
    sexo character(1),
    fechanacimiento date,
    tipo character(3),
    fecha date,
    users character(30),
    hora time without time zone
);
     DROP TABLE public.reg_clientes;
       public            postgres    false            �            1259    17092    reg_clientes_reg_id_seq    SEQUENCE     �   CREATE SEQUENCE public.reg_clientes_reg_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.reg_clientes_reg_id_seq;
       public          postgres    false    205            w           0    0    reg_clientes_reg_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.reg_clientes_reg_id_seq OWNED BY public.reg_clientes.reg_id;
          public          postgres    false    204            �            1259    16422 
   sucursales    TABLE     -  CREATE TABLE public.sucursales (
    sucnombre character(30) NOT NULL,
    succiudad character(30) NOT NULL,
    succalle character(30) NOT NULL,
    sucactivo numeric(11,2) NOT NULL,
    sucgerente character(50) NOT NULL,
    suctelefono character(10) NOT NULL,
    sucfechacreacion date NOT NULL
);
    DROP TABLE public.sucursales;
       public            postgres    false            x           0    0    TABLE sucursales    ACL       GRANT SELECT ON TABLE public.sucursales TO gerente;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sucursales TO jefe;
GRANT SELECT,UPDATE ON TABLE public.sucursales TO cajero;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sucursales TO programador;
          public          postgres    false    200            �            1259    16428    tiene    TABLE     i   CREATE TABLE public.tiene (
    cuenumero character(7) NOT NULL,
    clinombre character(30) NOT NULL
);
    DROP TABLE public.tiene;
       public            postgres    false            �            1259    17063    users    TABLE     �   CREATE TABLE public.users (
    user_id integer NOT NULL,
    usuario character(50) NOT NULL,
    "contraseña" character(50) NOT NULL
);
    DROP TABLE public.users;
       public            postgres    false            �
           2604    17107    empleados_insert reg_id    DEFAULT     �   ALTER TABLE ONLY public.empleados_insert ALTER COLUMN reg_id SET DEFAULT nextval('public.empleados_insert_reg_id_seq'::regclass);
 F   ALTER TABLE public.empleados_insert ALTER COLUMN reg_id DROP DEFAULT;
       public          postgres    false    207    206    207            �
           2604    17097    reg_clientes reg_id    DEFAULT     z   ALTER TABLE ONLY public.reg_clientes ALTER COLUMN reg_id SET DEFAULT nextval('public.reg_clientes_reg_id_seq'::regclass);
 B   ALTER TABLE public.reg_clientes ALTER COLUMN reg_id DROP DEFAULT;
       public          postgres    false    205    204    205            \          0    16394    clientes 
   TABLE DATA           h   COPY public.clientes (clinombre, clicalle, cliciudad, clisexo, clifechanacimiento, clitipo) FROM stdin;
    public          postgres    false    196   �Z       ]          0    16400    cuentas 
   TABLE DATA           T   COPY public.cuentas (cuenumero, sucnombre, cuesaldo, cuetipo, cuefecha) FROM stdin;
    public          postgres    false    197   �\       ^          0    16407    debe 
   TABLE DATA           4   COPY public.debe (prenumero, clinombre) FROM stdin;
    public          postgres    false    198   �]       g          0    17104    empleados_insert 
   TABLE DATA           V   COPY public.empleados_insert (reg_id, nombreempleado, fecha, users, hora) FROM stdin;
    public          postgres    false    207   ^       c          0    17081    long 
   TABLE DATA           6   COPY public.long (usuario, fecha, tiempo) FROM stdin;
    public          postgres    false    203   9^       _          0    16415 	   prestamos 
   TABLE DATA           a   COPY public.prestamos (prenumero, sucnombre, preimporte, prefecha, fechavencimiento) FROM stdin;
    public          postgres    false    199   �^       e          0    17094    reg_clientes 
   TABLE DATA           }   COPY public.reg_clientes (reg_id, nombrecliente, calle, ciudad, sexo, fechanacimiento, tipo, fecha, users, hora) FROM stdin;
    public          postgres    false    205   [_       `          0    16422 
   sucursales 
   TABLE DATA           z   COPY public.sucursales (sucnombre, succiudad, succalle, sucactivo, sucgerente, suctelefono, sucfechacreacion) FROM stdin;
    public          postgres    false    200   `       a          0    16428    tiene 
   TABLE DATA           5   COPY public.tiene (cuenumero, clinombre) FROM stdin;
    public          postgres    false    201   �a       b          0    17063    users 
   TABLE DATA           @   COPY public.users (user_id, usuario, "contraseña") FROM stdin;
    public          postgres    false    202   ?b       y           0    0    empleados_insert_reg_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.empleados_insert_reg_id_seq', 1, false);
          public          postgres    false    206            z           0    0    reg_clientes_reg_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.reg_clientes_reg_id_seq', 1, false);
          public          postgres    false    204            �
           2606    17109 &   empleados_insert empleados_insert_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.empleados_insert
    ADD CONSTRAINT empleados_insert_pkey PRIMARY KEY (reg_id);
 P   ALTER TABLE ONLY public.empleados_insert DROP CONSTRAINT empleados_insert_pkey;
       public            postgres    false    207            �
           2606    16398    clientes pk_clientes 
   CONSTRAINT     Y   ALTER TABLE ONLY public.clientes
    ADD CONSTRAINT pk_clientes PRIMARY KEY (clinombre);
 >   ALTER TABLE ONLY public.clientes DROP CONSTRAINT pk_clientes;
       public            postgres    false    196            �
           2606    16404    cuentas pk_cuentas 
   CONSTRAINT     W   ALTER TABLE ONLY public.cuentas
    ADD CONSTRAINT pk_cuentas PRIMARY KEY (cuenumero);
 <   ALTER TABLE ONLY public.cuentas DROP CONSTRAINT pk_cuentas;
       public            postgres    false    197            �
           2606    16411    debe pk_debe 
   CONSTRAINT     \   ALTER TABLE ONLY public.debe
    ADD CONSTRAINT pk_debe PRIMARY KEY (prenumero, clinombre);
 6   ALTER TABLE ONLY public.debe DROP CONSTRAINT pk_debe;
       public            postgres    false    198    198            �
           2606    16419    prestamos pk_prestamos 
   CONSTRAINT     [   ALTER TABLE ONLY public.prestamos
    ADD CONSTRAINT pk_prestamos PRIMARY KEY (prenumero);
 @   ALTER TABLE ONLY public.prestamos DROP CONSTRAINT pk_prestamos;
       public            postgres    false    199            �
           2606    16426    sucursales pk_sucursales 
   CONSTRAINT     ]   ALTER TABLE ONLY public.sucursales
    ADD CONSTRAINT pk_sucursales PRIMARY KEY (sucnombre);
 B   ALTER TABLE ONLY public.sucursales DROP CONSTRAINT pk_sucursales;
       public            postgres    false    200            �
           2606    16432    tiene pk_tiene 
   CONSTRAINT     ^   ALTER TABLE ONLY public.tiene
    ADD CONSTRAINT pk_tiene PRIMARY KEY (cuenumero, clinombre);
 8   ALTER TABLE ONLY public.tiene DROP CONSTRAINT pk_tiene;
       public            postgres    false    201    201            �
           2606    17099    reg_clientes reg_clientes_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.reg_clientes
    ADD CONSTRAINT reg_clientes_pkey PRIMARY KEY (reg_id);
 H   ALTER TABLE ONLY public.reg_clientes DROP CONSTRAINT reg_clientes_pkey;
       public            postgres    false    205            �
           2606    17067    users users_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (user_id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public            postgres    false    202            �
           1259    16399    clientes_pk    INDEX     L   CREATE UNIQUE INDEX clientes_pk ON public.clientes USING btree (clinombre);
    DROP INDEX public.clientes_pk;
       public            postgres    false    196            �
           1259    16421 
   concede_fk    INDEX     E   CREATE INDEX concede_fk ON public.prestamos USING btree (sucnombre);
    DROP INDEX public.concede_fk;
       public            postgres    false    199            �
           1259    16405 
   cuentas_pk    INDEX     J   CREATE UNIQUE INDEX cuentas_pk ON public.cuentas USING btree (cuenumero);
    DROP INDEX public.cuentas_pk;
       public            postgres    false    197            �
           1259    16413    debe_cli_fk    INDEX     A   CREATE INDEX debe_cli_fk ON public.debe USING btree (clinombre);
    DROP INDEX public.debe_cli_fk;
       public            postgres    false    198            �
           1259    16412    debe_pk    INDEX     O   CREATE UNIQUE INDEX debe_pk ON public.debe USING btree (prenumero, clinombre);
    DROP INDEX public.debe_pk;
       public            postgres    false    198    198            �
           1259    16414    debe_pre_fk    INDEX     A   CREATE INDEX debe_pre_fk ON public.debe USING btree (prenumero);
    DROP INDEX public.debe_pre_fk;
       public            postgres    false    198            �
           1259    16406 	   genera_fk    INDEX     B   CREATE INDEX genera_fk ON public.cuentas USING btree (sucnombre);
    DROP INDEX public.genera_fk;
       public            postgres    false    197            �
           1259    16420    prestamos_pk    INDEX     N   CREATE UNIQUE INDEX prestamos_pk ON public.prestamos USING btree (prenumero);
     DROP INDEX public.prestamos_pk;
       public            postgres    false    199            �
           1259    16427    sucursales_pk    INDEX     P   CREATE UNIQUE INDEX sucursales_pk ON public.sucursales USING btree (sucnombre);
 !   DROP INDEX public.sucursales_pk;
       public            postgres    false    200            �
           1259    16434    tiene_cli_fk    INDEX     C   CREATE INDEX tiene_cli_fk ON public.tiene USING btree (clinombre);
     DROP INDEX public.tiene_cli_fk;
       public            postgres    false    201            �
           1259    16435    tiene_cue_fk    INDEX     C   CREATE INDEX tiene_cue_fk ON public.tiene USING btree (cuenumero);
     DROP INDEX public.tiene_cue_fk;
       public            postgres    false    201            �
           1259    16433    tiene_pk    INDEX     Q   CREATE UNIQUE INDEX tiene_pk ON public.tiene USING btree (cuenumero, clinombre);
    DROP INDEX public.tiene_pk;
       public            postgres    false    201    201            �
           2620    17091    sucursales log_insert    TRIGGER     m   CREATE TRIGGER log_insert AFTER INSERT ON public.sucursales FOR EACH ROW EXECUTE PROCEDURE public.prueba2();
 .   DROP TRIGGER log_insert ON public.sucursales;
       public          postgres    false    209    200            �
           2620    17101    clientes log_insert2    TRIGGER     k   CREATE TRIGGER log_insert2 AFTER UPDATE ON public.clientes FOR EACH ROW EXECUTE PROCEDURE public.prueba();
 -   DROP TRIGGER log_insert2 ON public.clientes;
       public          postgres    false    223    196            �
           2606    16436 "   cuentas fk_cuentas_genera_sucursal    FK CONSTRAINT     �   ALTER TABLE ONLY public.cuentas
    ADD CONSTRAINT fk_cuentas_genera_sucursal FOREIGN KEY (sucnombre) REFERENCES public.sucursales(sucnombre) ON UPDATE RESTRICT ON DELETE RESTRICT;
 L   ALTER TABLE ONLY public.cuentas DROP CONSTRAINT fk_cuentas_genera_sucursal;
       public          postgres    false    2766    197    200            �
           2606    16441    debe fk_debe_debe_cli_clientes    FK CONSTRAINT     �   ALTER TABLE ONLY public.debe
    ADD CONSTRAINT fk_debe_debe_cli_clientes FOREIGN KEY (clinombre) REFERENCES public.clientes(clinombre) ON UPDATE RESTRICT ON DELETE RESTRICT;
 H   ALTER TABLE ONLY public.debe DROP CONSTRAINT fk_debe_debe_cli_clientes;
       public          postgres    false    2751    196    198            �
           2606    16446    debe fk_debe_debe_pre_prestamo    FK CONSTRAINT     �   ALTER TABLE ONLY public.debe
    ADD CONSTRAINT fk_debe_debe_pre_prestamo FOREIGN KEY (prenumero) REFERENCES public.prestamos(prenumero) ON UPDATE RESTRICT ON DELETE RESTRICT;
 H   ALTER TABLE ONLY public.debe DROP CONSTRAINT fk_debe_debe_pre_prestamo;
       public          postgres    false    198    2763    199            �
           2606    16451 &   prestamos fk_prestamo_concede_sucursal    FK CONSTRAINT     �   ALTER TABLE ONLY public.prestamos
    ADD CONSTRAINT fk_prestamo_concede_sucursal FOREIGN KEY (sucnombre) REFERENCES public.sucursales(sucnombre) ON UPDATE RESTRICT ON DELETE RESTRICT;
 P   ALTER TABLE ONLY public.prestamos DROP CONSTRAINT fk_prestamo_concede_sucursal;
       public          postgres    false    2766    199    200            �
           2606    16456 !   tiene fk_tiene_tiene_cli_clientes    FK CONSTRAINT     �   ALTER TABLE ONLY public.tiene
    ADD CONSTRAINT fk_tiene_tiene_cli_clientes FOREIGN KEY (clinombre) REFERENCES public.clientes(clinombre) ON UPDATE RESTRICT ON DELETE RESTRICT;
 K   ALTER TABLE ONLY public.tiene DROP CONSTRAINT fk_tiene_tiene_cli_clientes;
       public          postgres    false    196    2751    201            �
           2606    16461     tiene fk_tiene_tiene_cue_cuentas    FK CONSTRAINT     �   ALTER TABLE ONLY public.tiene
    ADD CONSTRAINT fk_tiene_tiene_cue_cuentas FOREIGN KEY (cuenumero) REFERENCES public.cuentas(cuenumero) ON UPDATE RESTRICT ON DELETE RESTRICT;
 J   ALTER TABLE ONLY public.tiene DROP CONSTRAINT fk_tiene_tiene_cue_cuentas;
       public          postgres    false    2755    201    197            \   �  x����R�0���S��؆��褄i($�3ezY,��Ȗg��p��#��*���79�Ƿ]�ӿ��z�q��h�C�Zr��(%c�U	h��d��p'��K��O�1�G�����u�ia�C�_49�����1��� {�M�Q�^1�D 3���?�J��
ɤ����N�y2������@ȡ�;��B�<��j�t��FI��,��B�MO_j�^6�r/w�n0V8 
.�
��j��V�_��0+��w��&�[��G3��T3�V������ ����J(g�Gݛ�j�#z�����F�{��_���D�m���nՂYO� c��N�8�xp�̓��V�׾E��<R,��椠.��p����ZG�/�i�~�ﮅϿ|�.��m/�6�J�.��@�;Z�+0}�޶T:� ��/�謼�k����ʚ��sx��']>��������iZ|�R»JZɭ��⃝|?�~~H����`�      ]   �   x���1� �N�$�=@��0tr�܅TӘXm��Yz���%^����K W�4�G׆���ԪB�� ��-}i#��S�FaQ+cD�rt'"5a�[2������ny��5g˜H����9�=~{�`�I�����6��xaR����5�қ��?���ϵ���vJ��<:���~���dڳ'��]����<��v-�\pUR�/z�e�      ^   _   x��54T�t�+�,,M�R�
�tM8�sR2�2S��q)2U�8���9PUfD�2'�UF�D(�4&���D(YG�� �B�R�q*����� KI�      g      x������ � �      c   <   x��I��K,J�W p���p��q�Pd���������������!��b���� z��      _   �   x���=�0�99pd�2V�+q�.Qa@�P��_�IFQ�,���z j�c�m�g���訄QY$$�����|������ v�R�cN����v"�e������=`�<p�"�k��	A�"`[�d��^o�h�DO=�-�Q�ȼM�W�O�B����k�×&�9��!�e,���,J�]�$���4Z�?�BS�      e   �   x���;�0D��)r[���sh�4�4Nb�H��G��)0�n�4z՝<r�=P�	��܌S�6d�k�����"�HK�T]@�F��4�������3�a��M)#�������h��s�ѷð�B����kT��XEsS��B| �L�      `   �  x�͕�n�0���*z3��#���d�ӡ������Im�/gײ[U0`�L�l8{iy��m�s��P��^
���P���rJ)�&�b��f�b��؊���v���1Q�u}Yc�����L�c`��y����*X��Tf4G��z�3mMi�����vB�Nr�>�ZY���(��"ɥ�ǽ�v={����z^��z%����>d�>�T�ې8��x��z�*�����:���D�jd�2@��@Q_/b��v6[��U�nU\)ReF7�F�;R)�!ey���	δ�h�7�=����T�lFy���YӌrBc��	.	q��]̍a��)�z3�95I��e<N��xɴ��R�j�6.�_���Km9$���[�R�!G��dv�|��#�h14���e[���T�r�X���c��4S:      a   m   x�s�540TP��J,H,IT���u�M��B�R��(340*s�+�,,M�©����(K�n#h��Q�����������%���T��>�AK����=... �TI      b      x������ � �     