Clientes
insert into CLIENTE VALUES(1	,'1723154680'	,'Juan Diego	Acurio		','Quito		','0993657415','	juandi@hotmial.com		','La Gasca		','SI');
insert into CLIENTE VALUES(2	,'1756984763'	,'Sebatian	Cabrera			','Cuenca		','0977445586','	sebatiandi@hotmial.com	','San Sebastian','SI');
insert into CLIENTE VALUES(3	,'1790814840'	,'Camila	Quintero		','Manta		','0996332255','	camiladi@hotmial.com	','Santa Clara	','SI');
insert into CLIENTE VALUES(4	,'1724644921'	,'Jose 	Perez				','Guayaquil	','0987563214','	josedi@hotmial.com		','Urdesa		','SI');
insert into CLIENTE VALUES(5	,'0458475012'	,'Sheila	Romero			','Quito		','0996541299','	sheiladi@hotmial.com	','San Blas		','SI');
insert into CLIENTE VALUES(6	,'0592305091'	,'Jose 	Peralta				','Quito		','0963257888','	josedi@hotmial.com		','Pomasqui		','SI');
insert into CLIENTE VALUES(7	,'1726135170'	,'Carlos 	Daza			','Quito		','0963215478','	carlosdi@hotmial.com	','Calderon		','NO');
insert into CLIENTE VALUES(8	,'0959965261'	,'Alexis	Padilla			','Guayaquil	','0963215487','	alexisdi@hotmial.com	','Santa Ana	','SI');
insert into CLIENTE VALUES(9	,'1793795343'	,'Micaela	Echeverria		','Manta		','0986354715','	micaeladi@hotmial.com	','Jipijapa		','NO');
insert into CLIENTE VALUES(10	,'1727625420'	,'Andres	Alquinga		','Cuenca		','0987456321','	andresdi@hotmial.com	','El Batan		','SI');

vete 

insert into VETERINARIOS VALUES(1	,'1723654781	','Carlos	Pacheco			','General	','0986321587	','carlosvet@gmail.com		','M	','SI');
insert into VETERINARIOS VALUES(2	,'1789632250	','Ricardo	Campoverde		','General	','0996325148	','ricardovet@gmail.com		','M	','SI');
insert into VETERINARIOS VALUES(3	,'1766559873	','Pamela	Silva			','General	','0996335584	','pamelavet@gmail.com		','F	','SI');
insert into VETERINARIOS VALUES(4	,'1700036983	','Alejandra	Camacho		','General	','0986324571	','alejandravet@gmail.com	','F	','NO');
insert into VETERINARIOS VALUES(5	,'1733369140	','Felipe	Espinoza		','General	','0936254488	','felipevet@gmail.com		','M	','SI');
insert into VETERINARIOS VALUES(6	,'1721600641	','Veronica	Leon			','General	','0995447814	','veronicavet@gmail.com	','F	','SI');
insert into VETERINARIOS VALUES(7	,'1714583986	','Sebastian	Echeverria	','General	','0963254781	','sebastianvet@gmail.com	','M	','SI');
insert into VETERINARIOS VALUES(8	,'1707567331	','Fiorella	Garzon			','General	','0985447763	','fiorellavet@gmail.com	','F	','SI');
insert into VETERINARIOS VALUES(9	,'1700550670	','Diego	Jaramillo		','General	','0999963555	','diegovet@gmail.com		','M	','NO');
insert into VETERINARIOS VALUES(10	,'1766887981	','Rafael	Romero			','General	','0988774456	','rafaelvet@gmail.com		','M	','SI');

CITAS

insert into CITAS VALUES(1	,10	,'13/10/2020	','La Gasca		','SI');
insert into CITAS VALUES(2	,6	,'25/11/2020	','San Blas		','SI');
insert into CITAS VALUES(3	,5	,'26/11/2020	','Carcelen		','SI');
insert into CITAS VALUES(4	,10	,'27/11/2020	','Marianitas	','SI');
insert into CITAS VALUES(5	,6	,'28/11/2020	','El Inca		','SI');
insert into CITAS VALUES(6	,8	,'29/11/2020	','Conocoto		','SI');
insert into CITAS VALUES(7	,10	,'30/11/2020	','America		','SI');
insert into CITAS VALUES(8	,6	,'01/12/2020	','Quitumbe		','NO');
insert into CITAS VALUES(9	,1	,'02/12/2020	','El Batan		','SI');
insert into CITAS VALUES(10	,10	,'03/12/2020	','Sangolqui	','SI');

MASCOTAS
insert into MASCOTAS VALUES(1	,1	,'Scrappy	','16/2/2010	','Perro	','Golden		','SI	','SI');
insert into MASCOTAS VALUES(2	,2	,'Bodoque	','09/6/2017	','Perro	','Poodle		','SI	','SI');
insert into MASCOTAS VALUES(3	,3	,'Pelusa	','08/8/2013	','Gato		','Shorthair	','SI	','SI');
insert into MASCOTAS VALUES(4	,4	,'Rayo		','16/4/2016	','Perro	','Labrador		','SI	','SI');
insert into MASCOTAS VALUES(5	,5	,'Max		','16/5/2010	','Perro	','Beagle		','SI	','SI');
insert into MASCOTAS VALUES(6	,6	,'Kalua		','09/11/2017	','Gato		','Siberiano	','SI	','NO');
insert into MASCOTAS VALUES(7	,10	,'Scooby	','08/6/2014	','Perro	','Beagle		','SI	','SI');
insert into MASCOTAS VALUES(8	,4	,'Hachi		','16/4/2015	','Perro	','Poodle		','SI	','SI');
insert into MASCOTAS VALUES(9	,2	,'Bobby		','12/11/2010	','Gato		','Bombay		','SI	','SI');
insert into MASCOTAS VALUES(10	,5	,'Peluchin	','09/6/2019	','Perro	','Poodle		','SI	','NO');



FICHA_MEDICAS

insert into FICHA_MEDICAS VALUES(10	,1	,1	,'Dermatitis		','3 Parvovirus');
insert into FICHA_MEDICAS VALUES(6	,2	,2	,'Desparasitacion	','2 Moquillo');
insert into FICHA_MEDICAS VALUES(5	,3	,3	,'Obesidad			','1 Rabia');
insert into FICHA_MEDICAS VALUES(10	,4	,4	,'Chequeo Mensual	','1 Hepatitis');
insert into FICHA_MEDICAS VALUES(6	,5	,5	,'Dermatitis		','2 Rabia');
insert into FICHA_MEDICAS VALUES(8	,6	,6	,'Desparasitacion	','2 Triplefelina');
insert into FICHA_MEDICAS VALUES(10	,7	,7	,'Chequeo Mensual	','1 Mixomatosis');
insert into FICHA_MEDICAS VALUES(6	,8	,8	,'Chequeo Mensual	','3 Triplefelina');
insert into FICHA_MEDICAS VALUES(1	,9	,9	,'Obesidad			','3 Hepatitis');
insert into FICHA_MEDICAS VALUES(10	,10	,10	,'Chequeo Mensual	','1 Rabia');

RECETAS

insert into RECETAS VALUES(1	,10	,1	,'13/10/2020	','Scrappy		','Cefalexina		','Tomar una tableta cada 12 horas por 3 dias ');
insert into RECETAS VALUES(2	,6	,2	,'25/11/2020	','Bodoque		','Epamin			','Tomar dos tabletas cada 12 horas por 1 dia');
insert into RECETAS VALUES(3	,5	,3	,'26/11/2020	','Pelusa		','Sabril			','Tomar 1/2 tableta cada 12 horas por 2 dias');
insert into RECETAS VALUES(4	,10	,4	,'27/11/2020	','Rayo			','Albendazol		','Tomar 1 tableta ');
insert into RECETAS VALUES(5	,6	,5	,'28/11/2020	','Max			','Sabril			','Tomar 2 tableta cada 12 horas por 2 dias');
insert into RECETAS VALUES(6	,8	,6	,'29/11/2020	','Kalua		','Cefalexina		','Tomar una tableta cada 12 horas por 3 dias ');
insert into RECETAS VALUES(7	,10	,7	,'30/11/2020	','Scooby		','Albendazol		','Tomar 1 tableta ');
insert into RECETAS VALUES(8	,6	,8	,'1/12/2020		','Hachi		','Cefalexina		','Tomar una tableta cada 12 horas por 2 dias ');
insert into RECETAS VALUES(9	,1	,9	,'2/12/2020		','Bobby		','basken PLUS		','Tomar 1 tableta por 2 dias');
insert into RECETAS VALUES(10	,10	,10	,'3/12/2020		','Peluchin		','Albendazol		','Tomar 2 tabletas cada 6 horas por 3 dias');

FACTURAS
insert into FACTURAS VALUES('001-001	',1		,'0001-001-001	',11.2	,'14/10/2020');
insert into FACTURAS VALUES('001-002	',2		,'0001-001-002	',16.8	,'26/11/2020');
insert into FACTURAS VALUES('001-003	',3		,'0001-001-003	',11.2	,'27/11/2020');
insert into FACTURAS VALUES('001-004	',4		,'0001-001-004	',16.8	,'28/11/2020');
insert into FACTURAS VALUES('001-005	',5		,'0001-001-005	',5.6	,'29/11/2020');
insert into FACTURAS VALUES('001-006	',6		,'0001-001-006	',22.4	,'30/11/2020');
insert into FACTURAS VALUES('001-007	',7		,'0001-001-007	',28	,'2020-11-31');
insert into FACTURAS VALUES('001-008	',8		,'0001-001-008	',16.8	,'2/12/2020');
insert into FACTURAS VALUES('001-009	',9		,'0001-001-009	',11.2	,'3/12/2020');
insert into FACTURAS VALUES('001-010	',10	,'0001-001-010	',22.4	,'4/12/2020');


DETALLES_FACTURA


insert into DETALLES_FACTURA VALUES('001-001	','Consulta Medica			',2	,10	,11.2);
insert into DETALLES_FACTURA VALUES('001-002	','Consulta y Medicacion	',1	,15	,16.8);
insert into DETALLES_FACTURA VALUES('001-003	','Consulta Medica			',2	,10	,11.2);
insert into DETALLES_FACTURA VALUES('001-004	','Consulta y Medicacion	',1	,15	,16.8);
insert into DETALLES_FACTURA VALUES('001-005	','Consulta Medica			',1	,5	,5.6);
insert into DETALLES_FACTURA VALUES('001-006	','Consulta y Medicacion	',1	,20	,22.4);
insert into DETALLES_FACTURA VALUES('001-007	','Consulta Medica			',4	,25	,28.0);
insert into DETALLES_FACTURA VALUES('001-008	','Consulta y Medicacion	',1	,15	,16.8);
insert into DETALLES_FACTURA VALUES('001-009	','Consulta Medica			',2	,10	,11.2);
insert into DETALLES_FACTURA VALUES('001-010	','Consulta y Medicacion	',1	,20	,22.4);

