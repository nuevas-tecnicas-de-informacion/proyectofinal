@extends('layouts.app')

@section('content')

<div class="container">
<br/>
<table class="table table-light table-hover">

    <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>ID_Cliente</th>
            <th>ID_Veterinario</th>
            <th>Fecha</th>
            <th>Mascota</th>
            <th>Descripcion</th>
            <th>Indicaciones</th>
        </tr>
    </thead>

    <tbody>
    @foreach($recetas as $estaReceta)
        <tr>
            <td>{{$loop -> iteration}}</td>
            <td>{{$estaReceta -> ID_CLIENTE}}</td>
            <td>{{$estaReceta -> ID_VETERINARIO}}</td>
            <td>{{$estaReceta -> REC_FECHA}}</td>
            <td>{{$estaReceta -> REC_PACIENTES}}</td>
            <td>{{$estaReceta -> REC_DESCRIPCION}}</td>
            <td>{{$estaReceta -> REC_INDICACIONES}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
<!-- Nos srive para hacer la paginación de los datos -->
{{$recetas ->links()}}
</div>
@endsection