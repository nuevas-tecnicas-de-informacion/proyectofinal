@extends('layouts.app')

@section('content')

<div class="container">

<br/>
<table class="table table-light table-hover">

    <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>ID_Cita</th>
            <th>Cedula</th>
            <th>Nombre</th>
            <th>Ciudad</th>
            <th>Telefono</th>
            <th>Correo</th>
            <th>Dirección</th>
        </tr>
    </thead>

    <tbody>
    @foreach($clientes as $esteCliente)
        <tr>
            <td>{{$loop -> iteration}}</td>
            <td>{{$esteCliente -> ID_CITA}}</td>
            <td>{{$esteCliente -> CLI_CEDULA}}</td>
            <td>{{$esteCliente -> CLI_NOMBRES}} {{$esteCliente -> CLI_APELLIDO}} </td>
            <td>{{$esteCliente -> CLI_CIUDAD}}</td>
            <td>{{$esteCliente -> CLI_TELEFONO}}</td>
            <td>{{$esteCliente -> CLI_EMAIL}}</td>
            <td>{{$esteCliente -> CLI_DIRECCION}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
<!-- Nos srive para hacer la paginación de los datos -->
{{$clientes ->links()}}
</div>
@endsection