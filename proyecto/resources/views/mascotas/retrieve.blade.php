@extends('layouts.app')

@section('content')

<div class="container">

<br/>
<table class="table table-light table-hover">

    <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>ID_Mascota</th>
            <th>ID_Cliente</th>
            <th>Ficha Médica</th>
            <th>Nombre</th>
            <th>Edad</th>
            <th>Especie</th>
            <th>Raza</th>
        </tr>
    </thead>

    <tbody>
    @foreach($mascotas as $estaMascota)
        <tr>
            <td>{{$loop -> iteration}}</td>
            <td>{{$estaMascota -> ID_MASCOTA}}</td>
            <td>{{$estaMascota -> ID_CLIENTE}}</td>
            <td>{{$estaMascota -> ID_CARNET}} </td>
            <td>{{$estaMascota -> MASC_NOMBRE}}</td>
            <td>{{$estaMascota -> MASC_EDAD}}</td>
            <td>{{$estaMascota -> MASC_ESPECIE}}</td>
            <td>{{$estaMascota -> MASC_RAZA}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
<!-- Nos srive para hacer la paginación de los datos -->
{{$mascotas ->links()}}
</div>
@endsection