@extends('layouts.app')

@section('content')

<div class="container">

<br/>
<table class="table table-light table-hover">

    <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>ID_Cita</th>
            <th>ID_Veterinario</th>
            <th>Fecha</th>
            <th>Ubicación</th>
        </tr>
    </thead>

    <tbody>
    @foreach($citas as $estaCita)
        <tr>
            <td>{{$loop -> iteration}}</td>
            <td>{{$estaCita -> ID_CITA}}</td>
            <td>{{$estaCita -> ID_VETERINARIO}}</td>
            <td>{{$estaCita -> CITA_FECHA}}</td>
            <td>{{$estaCita -> CITA_UBICACION}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
<!-- Nos srive para hacer la paginación de los datos -->
{{$citas ->links()}}
</div>
@endsection