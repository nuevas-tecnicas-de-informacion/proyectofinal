@extends('layouts.app')

@section('content')

<div class="container">
<br/>
<table class="table table-light table-hover">

    <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>ID_Cita</th>
            <th>ID_Cliente</th>
            <th>RUC</th>
            <th>Nro Factura</th>
            <th>Detalle</th>
            <th>Nro Unidades</th>
            <th>Subtotal</th>
        </tr>
    </thead>

    <tbody>
    @foreach($facturas as $estaFactura)
        <tr>
            <td>{{$loop -> iteration}}</td>
            <td>{{$estaFactura -> ID_CLIENTE}}</td>
            <td>{{$estaFactura -> ID_CITA}}</td>
            <td>{{$estaFactura -> FAC_RUC}}</td>
            <td>{{$estaFactura -> FAC_NUMERO}}</td>
            <td>{{$estaFactura -> DETA_DESCRIPCION}}</td>
            <td>{{$estaFactura -> DETA_UNIDADES}}</td>
            <td>{{$estaFactura -> DETA_SUBTOTAL}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
<!-- Nos srive para hacer la paginación de los datos -->
{{$facturas ->links()}}
</div>
@endsection