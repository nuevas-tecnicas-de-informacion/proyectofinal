@extends('layouts.app')

@section('content')

<div class="container">

@if(Session::has('Mensaje'))
    <div class="alert alert-success" role="alert">
    {{ Session::get('Mensaje') }}
    </div>
@endif
<a href="{{url('veterinarios/create')}}" class="btn btn-success">Agregar Veterinario</a>
<br/>
<br/>
<table class="table table-light table-hover">

    <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>Cedula</th>
            <th>Nombre</th>
            <th>Especialidad</th>
            <th>Telefono</th>
            <th>Correo</th>
            <th>Acciones</th>
        </tr>
    </thead>

    <tbody>
    @foreach($veterinarios as $esteVeterinario)
        <tr>
            <td>{{$loop -> iteration}}</td>
            <td>{{$esteVeterinario -> VET_CEDULA}}</td>
            <td>{{$esteVeterinario -> VET_NOMBRE}} {{$esteVeterinario -> VET_APELLIDO}} </td>
            <td>{{$esteVeterinario -> VET_ESPECIALIDAD}}</td>
            <td>{{$esteVeterinario -> VET_TELEFONO}}</td>
            <td>{{$esteVeterinario -> VET_EMAIL}}</td>
            <td>
            <a class="btn btn-warning" href="{{ url('/veterinarios/'.$esteVeterinario->ID_VETERINARIO.'/edit') }}">Editar</a>
            <!-- Formulario para el botón borrar, usara el motodo post -->
            <form action="{{url('/veterinarios/'.$esteVeterinario->ID_VETERINARIO)}}" method="post" style="display:inline">
            {{csrf_field()}} <!-- Cuando se pasa la info de un formulario hay que usar un token -->
            {{method_field('DELETE')}} <!-- Que tipo de request pide este formulario al controlador -->
            <button class="btn btn-danger" type="submit" onClick="return confirm('¿Borrar?');";>Eliminar</button>
            <!-- Agregamos la acción onClick para lanzar una alerta con una funcion de js -->
            </form> 
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<!-- Nos srive para hacer la paginación de los datos -->
{{$veterinarios ->links()}}
</div>
@endsection