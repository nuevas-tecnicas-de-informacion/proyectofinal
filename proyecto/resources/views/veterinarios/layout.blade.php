<div class="form-group w-50">
    <label class="control-label" for="cedula"> {{'Cedula'}} </label>
    <input class="form-control" type="text" name="VET_CEDULA" id="cedula"
        value="{{ isset($veterinario -> VET_CEDULA) ? $veterinario -> VET_CEDULA:'' }}">
</div>

<div class="form-group w-50">
    <label class="control-label" for="nombre"> {{'Nombre'}} </label>
    <input class="form-control" type="text" name="VET_NOMBRE" id="nombre" 
        value="{{ isset($veterinario -> VET_NOMBRE) ? $veterinario -> VET_NOMBRE:'' }}">
</div>

<div class="form-group w-50">
    <label class="control-label" for="apellido"> {{'Apellido'}} </label>
    <input class="form-control" type="text" name="VET_APELLIDO" id="apellido" 
        value="{{ isset($veterinario -> VET_APELLIDO) ? $veterinario -> VET_APELLIDO:'' }}">
</div>

<div class="form-group w-50">
    <label class="control-label" for="especialidad"> {{'Especialidad'}} </label>
    <input class="form-control" type="text" name="VET_ESPECIALIDAD" id="especialidad" 
        value="{{ isset($veterinario -> VET_ESPECIALIDAD) ? $veterinario -> VET_ESPECIALIDAD:'' }}">
</div>

<div class="form-group w-50">
    <label class="control-label" for="telefono"> {{'Telefono'}} </label>
    <input class="form-control" type="text" name="VET_TELEFONO" id="telefono" 
        value="{{ isset($veterinario -> VET_TELEFONO) ? $veterinario -> VET_TELEFONO:'' }}">
</div>

<div class="form-group w-50">
    <label class="control-label" for="correo"> {{'E-Mail'}} </label>
    <input class="form-control" type="text" name="VET_EMAIL" id="correo" 
        value="{{ isset($veterinario -> VET_EMAIL) ? $veterinario -> VET_EMAIL:'' }}">
</div>

<input class="btn btn-success" type="submit" value="{{ $Modo == 'crear' ? 'Registrar':'Modificar'}}">
<a class="btn btn-primary" href="{{url('veterinarios')}}">Regresar</a>