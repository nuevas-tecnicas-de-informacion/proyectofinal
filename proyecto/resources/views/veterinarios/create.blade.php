@extends('layouts.app')

@section('content')
<form class="form-horizontal" action="{{url('/veterinarios')}}" method="post">
{{csrf_field()}}
@include('veterinarios.layout', ['Modo'=>'crear'])
</form>
@endsection