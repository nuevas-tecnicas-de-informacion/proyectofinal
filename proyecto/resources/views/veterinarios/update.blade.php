@extends('layouts.app')

@section('content')
<form action="{{ url('/veterinarios/'.$veterinario->ID_VETERINARIO) }}" method="post">
{{csrf_field()}} <!-- Cuando se pasa la info de un formulario hay que usar un token -->
{{method_field('PATCH')}} <!-- A traves de la url enviamos el metodo PATCH al controlador para acceder al metodo update de este -->
@include('veterinarios.layout', ['Modo'=>'modificar'])
</form>
@endsection