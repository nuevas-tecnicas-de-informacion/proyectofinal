@extends('layouts.app')

@section('content')

<div class="container">
<br/>
<table class="table table-light table-hover">

    <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>ID_Veterinario</th>
            <th>Epediente</th>
            <th>Ultima Consulta</th>
            <th>Esterilización</th>
            <th>Vacunas</th>
        </tr>
    </thead>

    <tbody>
    @foreach($fichas as $estaFicha)
        <tr>
            <td>{{$loop -> iteration}}</td>
            <td>{{$estaFicha -> ID_VETERINARIO}}</td>
            <td>{{$estaFicha -> CARNET_EXPEDIENTE}}</td>
            <td>{{$estaFicha -> CARNET_ULTIMAVISITA}}</td>
            <td>{{$estaFicha -> CARNET_ESTERILIZACION}}</td>
            <td>{{$estaFicha -> CARNET_VACUNAS}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
<!-- Nos srive para hacer la paginación de los datos -->
{{$fichas ->links()}}
</div>
@endsection