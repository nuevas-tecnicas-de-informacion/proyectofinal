<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clientes extends Model
{
    protected $table = 'cliente';
    protected $primaryKey = 'ID_CLIENTE';
    public $timestamps = false;
}
