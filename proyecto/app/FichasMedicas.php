<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FichasMedicas extends Model
{
    protected $table = 'carnet';
    protected $primaryKey = 'ID_CARNET';
    public $timestamps = false;
}
