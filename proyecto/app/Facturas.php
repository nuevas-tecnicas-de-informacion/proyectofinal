<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facturas extends Model
{
    protected $table = 'detallefactura';
    protected $primaryKey = 'ID_RECETAS';
    public $timestamps = false;
}
