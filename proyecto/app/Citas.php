<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Citas extends Model
{
    protected $table = 'citas';
    protected $primaryKey = 'ID_CITA';
    public $timestamps = false;
}
