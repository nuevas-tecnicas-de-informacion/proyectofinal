<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recetas extends Model
{
    protected $table = 'recetas';
    protected $primaryKey = 'ID_RECETAS';
    public $timestamps = false;
}
