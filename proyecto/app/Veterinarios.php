<?php

namespace App;

use App\Veterinarios;
use Illuminate\Database\Eloquent\Model;

class Veterinarios extends Model
{
    protected $table = 'veterinario';
    protected $primaryKey = 'ID_VETERINARIO';
    public $timestamps = false;
}
