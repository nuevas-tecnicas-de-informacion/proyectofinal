<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mascotas extends Model
{
    protected $table = 'mascotas';
    protected $primaryKey = 'ID_MASCOTA';
    public $timestamps = false;
}
