<?php

namespace App\Http\Controllers;

use App\FichasMedicas;
use Illuminate\Http\Request;

class FichasMedicasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos['fichas'] = FichasMedicas::paginate(10); //muestra las filas en grupos de n
        return view('fichasMedicas.retrieve', $datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FichasMedicas  $fichasMedicas
     * @return \Illuminate\Http\Response
     */
    public function show(FichasMedicas $fichasMedicas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FichasMedicas  $fichasMedicas
     * @return \Illuminate\Http\Response
     */
    public function edit(FichasMedicas $fichasMedicas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FichasMedicas  $fichasMedicas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FichasMedicas $fichasMedicas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FichasMedicas  $fichasMedicas
     * @return \Illuminate\Http\Response
     */
    public function destroy(FichasMedicas $fichasMedicas)
    {
        //
    }
}
