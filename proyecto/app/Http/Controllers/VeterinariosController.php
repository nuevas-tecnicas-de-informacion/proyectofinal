<?php

namespace App\Http\Controllers;

use App\Veterinarios;
use Illuminate\Http\Request;

class VeterinariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos['veterinarios'] = Veterinarios::paginate(10); //muestra las filas en grupos de n
        return view('veterinarios.retrieve', $datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('veterinarios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datosVeterinario = request() -> except('_token');
        Veterinarios::insert($datosVeterinario);
        return redirect('veterinarios')->with('Mensaje', 'Veterinario agregado con éxito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Veterinarios  $veterinarios
     * @return \Illuminate\Http\Response
     */
    public function show(Veterinarios $veterinarios)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Veterinarios  $veterinarios
     * @return \Illuminate\Http\Response
     */
    public function edit($id_veterinario)
    {
        $veterinario = Veterinarios::findOrFail($id_veterinario);
        //el metodo findOrFail recoge toda la info que corresponde a ese identificador
        return view('veterinarios.update', compact('veterinario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Veterinarios  $veterinarios
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_veterinario)
    {
        $datosVeterinario = request() -> except(['_token', '_method']);
        //por la url se reciben todos los inputs del formulario sin embargo exceptuamos el token
        //y el metodo por el cual llegamos a update
        Veterinarios::where('ID_VETERINARIO', '=', $id_veterinario)->update($datosVeterinario);
        //el metodo where va a actualizar de acuerdo al id y va a preguntar si ese campo es igual al
        //identificador que se ha enviado
        return redirect('veterinarios')->with('Mensaje', 'Veterinario modificado con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Veterinarios  $veterinarios
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_veterinario)
    {
        //En vez de recibir todo el objeto veterinario, solo queremos el identificador que se manda por url
        Veterinarios::destroy($id_veterinario);
        return redirect('veterinarios')->with('Mensaje', 'Veterinario eliminado');
    }
}
