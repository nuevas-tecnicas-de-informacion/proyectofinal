<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('veterinarios', 'VeterinariosController');
Route::get('/clientes', 'ClientesController@index');
Route::get('/citas', 'CitasController@index');
Route::get('/mascotas', 'MascotasController@index');
Route::get('/fichas', 'FichasMedicasController@index');
Route::get('/recetas', 'RecetasController@index');
Route::get('/facturas', 'FacturasController@index');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
