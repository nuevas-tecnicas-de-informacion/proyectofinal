VETERINARIO

insert into VETERINARIO values(1	,'1723461581',	'Juan Carlos',	'Chamorro',	'Cirugia',		'2573011	',	'juancham@gmail.com');
insert into VETERINARIO values(2	,'0512487963',	'Fabian' ,		'Bonilla',	'Fisioterapia',	'0982547895	',	'fibianbo@hotmail.com');
insert into VETERINARIO values(3	,'1789564723',	'Diego',		'Jaramillo'	,	'Anestesiologa','2698741	',	'jaradie@gmail.com');
insert into VETERINARIO values(4	,'1736894510',	'Camila',	'	Quintero',	'Cirugia',		'2698741	',	'quintcami@gmail.com');
insert into VETERINARIO values(6	,'0459786310',	'Steven',	'Puente',		'Anestesiologa','0974563322	',	'puetestev@hotmail.com');
insert into VETERINARIO values(7	,'1789457412',	'Kevin',	'Sandoval',		'Fisioterapia',	'0944556677	',	'kevosand@gmail.com');
insert into VETERINARIO values(8	,'1789654231',	'Vivien',	'Cruz'	,		'Fisioterapia',	'0911445577	',	'vivicruz@gmail.com');
insert into VETERINARIO values(9	,'1789451231',	'Katherin',	'Matute',		'Cirugia',		'0978451236	',	'matutkat@hotmail.com');
insert into VETERINARIO values(10	,'1700112233',	'Andres',	'Alquinga',		'Anestesiologa','0936987412	',	'andy89@hotmail.com');
insert into VETERINARIO values(11	,'1744556690',	'Carlos',	'Daza'	,		'Fisioterapia',	'0945886630	',	'carlitos098@gmail.com');
insert into VETERINARIO values(12	,'1766998873',	'Esteban',	'Velasquez',	'Cirugia',		'2698417	',	'estevan@gamil.com');
insert into VETERINARIO values(13	,'0588993640',	'Alberto',	'Viteri',		'Fisioterapia',	'3214569	',	'albertvi7@hotmail.com');
insert into VETERINARIO values(14	,'0489784511',	'Sebastian','	Cabrera',	'Anestesiologa','3621478	',	'sebas87@hotmail.com');
insert into VETERINARIO values(15	,'1789655513',	'Romina',	'Gomez'	,		'Fisioterapia',	'0948756385	',	'romygg@gmail.com');
insert into VETERINARIO values(16	,'1356897410',	'Stefanny',	'Hurtado',		'Cirugia',		'0982562695	',	'tefahurt7@hotmail.com');
insert into VETERINARIO values(17	,'1356441110',	'Daniel', 'Correa'	,		'Cirugia',		'3126599	',	'correadani@gmail.com');
insert into VETERINARIO values(18	,'1356987413',	'Juan Diego',	'Acurio',	'Anestesiologa','3678100	',	'acuriojuan@gmail.com');
insert into VETERINARIO values(19	,'0987546312',	'Mario'	'Castillo',			'Fisioterapia',	'3030300	',	'castillomari@gmail.com');
insert into VETERINARIO values(20	,'0489325581',	'Ricardo',	'Echeverria',	'Cirugia',		'0988789878	',	'echeverriarica@gmail.com');
CITAS

insert into CITAS VALUES(1,	1,	'21/9/2020',	'San Juan');
insert into CITAS VALUES(2,	16,	'21/9/2020',	'Calderon');
insert into CITAS VALUESVALUES(3,	20,	'22/9/2020',	'Sangolqui');
insert into CITAS VALUES(4,	3,	'23/9/2020',	'La Mariscal');
insert into CITAS VALUES(5,	12,	'24/9/2020',	'Pomasqui');
insert into CITAS VALUES(6,	9,	'24/9/2020',	'Carcelen');
insert into CITAS VALUES(7,	9,	'24/9/2020',	'El Batan');
insert into CITAS VALUES(8,	16,	'25/9/2020',	'De las Begonias N12-2 ');
insert into CITAS VALUES(9,	12,	'25/9/2020',	'El Batan');
insert into CITAS VALUES(10,	3,	'26/9/2020',	'San Rafael');
insert into CITAS VALUES(11,	20,	'27/9/2020',	'Quito Tenis');
insert into CITAS VALUES(12,	2,	'28/9/2020',	'Iñaquito');
insert into CITAS VALUES(13,	2,	'28/9/2020',	'Miraflores');
insert into CITAS VALUES(14,	9,	'28/9/2020',	'El Inca');
insert into CITAS VALUES(15,	1,	'29/9/2020',	'Las casas');
insert into CITAS VALUES(16,	20,	'29/9/2020',	'Carcelen');
insert into CITAS VALUES(17,	16,	'30/9/2020',	'La Mariscal');
insert into CITAS VALUES(18,	12,	'30/9/2020',	'Marianitas');
insert into CITAS VALUES(19,	3,	'30/9/2020',	'El Batan');
insert into CITAS VALUES(20,	1,	'1/10/2020',	'El Bosque');

CLIENTE

insert into CLIENTE VALUES(1,		1,	'1723431621',	'Gabriel	', 	'Campos		',	'Quito	'	,'2573011		','gaboc@hotmail.com	','Pineda y Haiti');
insert into CLIENTE VALUES(2,		2,	'1700035780',	'Brian		',	'Rodriguez	',	'Quito	'	,'2987416		','brianr@hotmail.com		','Habana y New York');
insert into CLIENTE VALUES(3,		3,	'1702376664',	'Ariel		',	'Pozo		',	'Quito	'	,'0985469751	','arielpo@gamil.com			','Cuba y America');
insert into CLIENTE VALUES(4,		4,	'1708241763',	'Bianca		',	'Echeverria	',	'Quito	'	,'0986321475	','biancaec@hotmail.com		','Rio de Janeiro y America');
insert into CLIENTE VALUES(5,		5,	'0507964512',	'Rodrigo	',	'Jaramillo	',	'Latacunga'	,'0963215478	','rodrigo@gamil.com			','Calle Quitus');
insert into CLIENTE VALUES(6,		6,	'0578963142',	'Angie		',	'Basantes	',	'Latacunga'	,'3698741		','angiebasa@hotmail.com		','Velasco Ibarra y Luis Cordero');
insert into CLIENTE VALUES(7,		7,	'0945785630',	'Alexis		',	'Paez		',	'Quito	'	,'0963332545	','alexispaz@hotmail.com		','Alfredo Pareja y Ficus');
insert into CLIENTE VALUES(8,		8,	'0478564131',	'Patricio	',	'Narvaez	',	'Quito	'	,'0987456321	','patonar@gmail.com			','Av.10 de Agosto');
insert into CLIENTE VALUES(9,		9,	'1389475612',	'Augusta	',	'Nevarez	',	'Ibarra	'	,'0963548712	','nevarezau@hotmail.com		','Juan Leon Mera y Patria');
insert into CLIENTE VALUES(10,	10,	'0987415470',	'Sofia		',	'Checa		',	'Quito	'	,'3698741		','sofia@hotmail.com			','Mariano Cardenal y Barrazueta');
insert into CLIENTE VALUES(11,	11,	'1387850143',	'Alvaro		',	'Yanez		',	'Quito	'	,'0963154875	','alvaroya78@hotmail.com	','Juan de Selis y Cardenal');
insert into CLIENTE VALUES(12,	12,	'1744556671',	'Javier		',	'Cruz		',	'Quito	'	,'0935487612	','javierc8@gmail.com		','Real Audencia y Dalmau');
insert into CLIENTE VALUES(13,	13,	'1788111100',	'Ricardo	',	'Campoverde	',	'Quito	'	,'0963558874	','rickic@hotmail.com		','Juan Larrea y Montufar');
insert into CLIENTE VALUES(14,	14,	'1777884451',	'Carlos		',	'Melendez	',	'Quito	'	,'2698741		','carlosec@gmial.com		','Ventimilla y Tamayo');
insert into CLIENTE VALUES(15,	15,	'1766445571',	'Kevin		',	'Flores		',	'Quito	'	,'0966558877	','kevin89@gmail.com			','Isla Seymor y San Cristobal');
insert into CLIENTE VALUES(16,	16,	'1733667710',	'Fernando	',	'Guanoquiza	',	'Quito	'	,'0933554478	','fernigu@gmail.com			','Bogota y Marin');
insert into CLIENTE VALUES(17,	17,	'1789446633',	'Gabriela	',	'Moscoso	',	'Quito	'	,'3214789		','gabymosco@hotmail.com		','Sabanilla y Gala');
insert into CLIENTE VALUES(18,	18,	'0589746310',	'Daniela	',	'Salvador	',	'Latacunga'	,'0933556677	','dani89sa@hotmail.com		','Simon Bolivar y Valencia');
insert into CLIENTE VALUES(19,	19,	'0988997740',	'Diego		',	'Pino		',	'Quito	'	,'0999999974	','diegopi@hotmail.com		','De os claveles y Begonias');
insert into CLIENTE VALUES(20,	20,	'0987456322',	'Victor		',	'Arauz		',	'Quito	'	,'2693415		','victor89@gmail.com		','La independencia');

CARNET


insert into CARNET VALUES(1	,1	,'Dermatitis		','22/3/2020	',S,	'3 Parvovirus');
insert into CARNET VALUES(2	,16	,'Desparasitacion	','22/3/2020	',N,	'2 Moquillo');
insert into CARNET VALUES(3	,20	,'Obesidad			','1/2/2020		',S,	'1 Rabia');
insert into CARNET VALUES(4	,3	,'Chequeo Mensual	','8/2/2020		',N,	'Ninguna');
insert into CARNET VALUES(5	,12	,'Chequeo Mensual	','29/2/2020	',N,	'1 Hepatitis');
insert into CARNET VALUES(6	,9	,'Dermatitis		','3/1/2020		',S,	'2  Rabia');
insert into CARNET VALUES(7	,9	,'Desparasitacion	','28/1/2020	',S,	'2 Triplefelina');
insert into CARNET VALUES(8	,16	,'Chequeo Mensual	','9/10/2019	',N,	'1 Mixomatosis');
insert into CARNET VALUES(9	,12	,'Chequeo Mensual	','12/12/2019	',S,	'3 Triplefelina');
insert into CARNET VALUES(10	,3	,'Obesidad			','6/12/2019	',N,	'4 Rabia');
insert into CARNET VALUES(11	,20	,'Chequeo Mensual	','10/11/2019	',N,	'3 Hepatitis');
insert into CARNET VALUES(12	,2	,'Dermatitis		','15/1/2020	',S,	'1 Rabia');
insert into CARNET VALUES(13	,2	,'Chequeo Mensual	','4/3/2020		',S,	'1 Triplefelina');
insert into CARNET VALUES(14	,9	,'Chequeo Mensual	','18/3/2020	',N,	'Ninguna');
insert into CARNET VALUES(15	,1	,'Desparasitacion	','14/2/2020	',S,	'1 Rabia');
insert into CARNET VALUES(16	,20	,'Chequeo Mensual	','4/2/2020		',N,	'1 Triplefelina');
insert into CARNET VALUES(17	,16	,'Dermatitis		','20/1/2020	',S,	'Rabia');
insert into CARNET VALUES(18	,12	,'Chequeo Mensual	','10/1/2020	',S,	'Rabia');
insert into CARNET VALUES(19	,3	,'Desparasitacion	','1/3/2020		',N,	'1 Mixomatosis');
insert into CARNET VALUES(20	,1	,'Dermatitis		','1/3/2020		',S,	'3 Rabia');


MASCOTAS

insert into MASCOTAS VALUES(1		,1	,1	,'Scrappy	',5	,'Perro		','Golden Retriever ');
insert into MASCOTAS VALUES(2		,2	,2	,'Peluchin	',2	,'Perro		','Pastor Aleman');
insert into MASCOTAS VALUES(3		,3	,3	,'Scooby	',1	,'Gato		','Persa');
insert into MASCOTAS VALUES(4		,4	,4	,'Boby		',3	,'Conejo	','Holandes');
insert into MASCOTAS VALUES(5		,5	,5	,'Lucas		',2	,'Perro		','Bulldog');
insert into MASCOTAS VALUES(6		,6	,6	,'Serafin	',2	,'Gato		','Siames');
insert into MASCOTAS VALUES(7		,7	,7	,'Max		',4	,'Gato		','Siames');
insert into MASCOTAS VALUES(8		,8	,8	,'Calua		',1	,'Conejo	','Holandes');
insert into MASCOTAS VALUES(9		,9	,9	,'Coco		',4	,'Gato		','Burmes');
insert into MASCOTAS VALUES(10	,10	,10	,'Malu		',3	,'Perro		','Bulldog');
insert into MASCOTAS VALUES(11	,11	,11	,'Violeta	',4	,'Perro		','Poodle');
insert into MASCOTAS VALUES(12	,12	,12	,'Rosita	',2	,'Perro		','Poodle');
insert into MASCOTAS VALUES(13	,13	,13	,'Jane		',10,'Gato		','British Shorthair');
insert into MASCOTAS VALUES(14	,14	,14	,'Bruno		',1	,'Perro		','Poodle');
insert into MASCOTAS VALUES(15	,15	,15	,'Pluto		',12,'Perro		','Beagle');
insert into MASCOTAS VALUES(16	,16	,16	,'Rex		',2	,'Gato		','Siberiano');
insert into MASCOTAS VALUES(17	,17	,17	,'Ali		',5	,'Gato		','Siberiano');
insert into MASCOTAS VALUES(18	,18	,18	,'Bana		',2	,'Conejo	','Angora');
insert into MASCOTAS VALUES(19	,19	,19	,'Bony		',1	,'Conejo	','Angora');
insert into MASCOTAS VALUES(20	,20	,20	,'Mimi		',3	,'Perro		','Beagle');

RECETAS VALUES 


insert into RECETAS VALUES (1	,1	,1	,'21/9/2020	','Scrappy	','Cefalexina		','Tomar una tableta cada 12 horas por 3 dias ');
insert into RECETAS VALUES (2	,2	,16	,'21/9/2020	','Peluchin	','Epamin			','Tomar dos tabletas cada 12 horas por 1 dia');
insert into RECETAS VALUES (3	,3	,20	,'22/9/2020	','Scooby	','Sabril			','Tomar 1/2 tableta cada 12 horas por 2 dias');
insert into RECETAS VALUES (4	,4	,3	,'23/9/2020	','Boby		','Albendazol		','Tomar 1 tableta ');
insert into RECETAS VALUES (5	,5	,12	,'24/9/2020	','Lucas	','Sabril			','Tomar 2 tableta cada 12 horas por 2 dias');
insert into RECETAS VALUES (6	,6	,9	,'24/9/2020	','Serafin	','Cefalexina		','Tomar una tableta cada 12 horas por 3 dias ');
insert into RECETAS VALUES (7	,7	,9	,'24/9/2020	','Max		','Albendazol		','Tomar 1 tableta ');
insert into RECETAS VALUES (8	,8	,16	,'25/9/2020	','Calua	','Cefalexina		','Tomar una tableta cada 12 horas por 2 dias ');
insert into RECETAS VALUES (9	,9	,12	,'25/9/2020	','Coco		','basken PLUS		','Tomar 1 tableta por 2 dias');
insert into RECETAS VALUES (10	,10	,3	,'26/9/2020	','Malu		','Albendazol		','Tomar 2 tabletas cada 6 horas por 3 dias');
insert into RECETAS VALUES (11	,11	,20	,'27/9/2020	','Violeta	','Albendazol		','Tomar 2 tabletas cada 6 horas por 3 dias');
insert into RECETAS VALUES (12	,12	,2	,'28/9/2020	','Rosita	','basken PLUS		','Tomar 1 tableta por 2 dias');
insert into RECETAS VALUES (13	,13	,2	,'28/9/2020	','Jane		','Epamin			','Tomar dos tabletas cada 12 horas por 2 dias');
insert into RECETAS VALUES (14	,14	,9	,'28/9/2020	','Bruno	','Sabril			','Tomar 3 tableta cada 12 horas por 3 dias');
insert into RECETAS VALUES (15	,15	,1	,'29/9/2020	','Pluto	','basken PLUS		','Tomar 1 tableta por 2 dias');
insert into RECETAS VALUES (16	,16	,20	,'29/9/2020	','Rex		','Cefalexina		','Tomar una tableta cada 6 horas por 2 dias ');
insert into RECETAS VALUES (17	,17	,16	,'30/9/2020	','Ali		','Albendazol		','Tomar 3 tabletas cada 6 horas por 2 dias');
insert into RECETAS VALUES (18	,18	,12	,'30/9/2020	','Bana		','Epamin			','Tomar dos tabletas cada 6 horas por 2 dias');
insert into RECETAS VALUES (19	,19	,3	,'30/9/2020	','Bony		','Sabril			','Tomar 1/2 tableta cada 12 horas por 3 dias');
insert into RECETAS VALUES (20	,20	,1	,'1/10/2020	','Mimi		','Cefalexina		','Tomar una tableta cada 12 horas por 2 dias ');



insert into Detalle_Factura VALUES(1	,1	,'1723461581	','001-001-0012	','Consulta Medica		',10	,2	,11.2);
insert into Detalle_Factura VALUES(2	,2	,'512487963		','001-002-0013	','Consulta y Medicacion',15	,1	,16.8);
insert into Detalle_Factura VALUES(3	,3	,'1789564723	','001-001-0013	','Consulta Medica		',10	,2	,11.2);
insert into Detalle_Factura VALUES(4	,4	,'1736894510	','001-002-0014	','Consulta y Medicacion',15	,1	,16.8);
insert into Detalle_Factura VALUES(5	,5	,'1789140250	','001-001-0014	','Consulta Medica		',5		,1	,5.6);
insert into Detalle_Factura VALUES(6	,6	,'459786310		','001-002-0015	','Consulta y Medicacion',20	,1	,22.4);
insert into Detalle_Factura VALUES(7	,7	,'1789457412	','001-001-0015	','Consulta Medica		',25	,4	,28.0);
insert into Detalle_Factura VALUES(8	,8	,'1789654231	','001-002-0016	','Consulta y Medicacion',15	,1	,16.8);
insert into Detalle_Factura VALUES(9	,9	,'1789451231	','001-001-0016	','Consulta Medica		',10	,2	,11.2);
insert into Detalle_Factura VALUES(10	,10	,'1700112233	','001-002-0017	','Consulta y Medicacion',20	,1	,22.4);
insert into Detalle_Factura VALUES(11	,11	,'1744556690	','001-001-0017	','Consulta Medica		',17	,2	,18.9);
insert into Detalle_Factura VALUES(12	,12	,'1766998873	','001-002-0018	','Consulta y Medicacion',16	,1	,18.4);
insert into Detalle_Factura VALUES(13	,13	,'588993640		','001-001-0018	','Consulta Medica		',16	,1	,17.9);
insert into Detalle_Factura VALUES(14	,14	,'489784511		','001-002-0019	','Consulta y Medicacion',15	,2	,17.3);
insert into Detalle_Factura VALUES(15	,15	,'1789655513	','001-001-0019	','Consulta Medica		',15	,1	,16.8);
insert into Detalle_Factura VALUES(16	,16	,'1356897410	','001-002-0020	','Consulta y Medicacion',15	,1	,16.3);
insert into Detalle_Factura VALUES(17	,17	,'1356441110	','001-001-0020	','Consulta Medica		',14	,1	,15.7);
insert into Detalle_Factura VALUES(18	,18	,'1356987413	','001-002-0021	','Consulta y Medicacion',14	,1	,15.2);
insert into Detalle_Factura VALUES(19	,19	,'987546312		','001-001-0021	','Consulta Medica		',13	,1	,14.7);
insert into Detalle_Factura VALUES(20	,20	,'489325581		','001-002-0022	','Consulta y Medicacion',13	,1	,14.1);







