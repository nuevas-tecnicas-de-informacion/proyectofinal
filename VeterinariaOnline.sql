/*==============================================================*/
/* DBMS name:      MySQL 4.0                                    */
/* Created on:     9/18/2020 10:19:04 PM                        */
/*==============================================================*/


drop index GENERAR_FK on CARNET;

drop index POSEER_FK on CARNET;

drop table if exists CARNET;

drop index ATENDER_FK on CITAS;

drop index AGENDAR2_FK on CITAS;

drop table if exists CITAS;

drop index AGENDAR_FK on CLIENTE;

drop table if exists CLIENTE;

drop index DETALLEFACTURA_FK on DETALLEFACTURA;

drop index DETALLEFACTURA2_FK on DETALLEFACTURA;

drop table if exists DETALLEFACTURA;

drop index TIENE_FK on MASCOTAS;

drop table if exists MASCOTAS;

drop index REALIZA_FK on RECETAS;

drop index ASIGNAR_FK on RECETAS;

drop table if exists RECETAS;

drop table if exists VETERINARIO;

/*==============================================================*/
/* Table: CARNET                                                */
/*==============================================================*/
create table CARNET
(
   ID_CARNET                      int                            not null,
   ID_MASCOTA                     int                            not null,
   ID_VETERINARIO                 int                            not null,
   CARNET_EXPEDIENTE              varchar(100)                   not null,
   CARNET_ULTIMAVISITA            date                           not null,
   CARNET_ESTERILIZACION          char(1)                        not null,
   CARNET_VACUNAS                 varchar(100)                   not null,
   primary key (ID_CARNET)
)
type = InnoDB;

/*==============================================================*/
/* Index: POSEER_FK                                             */
/*==============================================================*/
create index POSEER_FK on CARNET
(
   ID_MASCOTA
);

/*==============================================================*/
/* Index: GENERAR_FK                                            */
/*==============================================================*/
create index GENERAR_FK on CARNET
(
   ID_VETERINARIO
);

/*==============================================================*/
/* Table: CITAS                                                 */
/*==============================================================*/
create table CITAS
(
   ID_CITA                        int                            not null,
   ID_CLIENTE                     int,
   ID_VETERINARIO                 int                            not null,
   CITA_FECHA                     date                           not null,
   CITA_UBICACION                 char(50)                       not null,
   primary key (ID_CITA)
)
type = InnoDB;

/*==============================================================*/
/* Index: AGENDAR2_FK                                           */
/*==============================================================*/
create index AGENDAR2_FK on CITAS
(
   ID_CLIENTE
);

/*==============================================================*/
/* Index: ATENDER_FK                                            */
/*==============================================================*/
create index ATENDER_FK on CITAS
(
   ID_VETERINARIO
);

/*==============================================================*/
/* Table: CLIENTE                                               */
/*==============================================================*/
create table CLIENTE
(
   ID_CLIENTE                     int                            not null,
   ID_CITA                        int                            not null,
   CLI_CEDULA                     char(10)                       not null,
   CLI_NOMBRES                    char(30)                       not null,
   CLI_APELLIDO                   char(30)                       not null,
   CLI_CIUDAD                     char(10)                       not null,
   CLI_TELEFONO                   char(13)                       not null,
   CLI_EMAIL                      varchar(20)                    not null,
   CLI_DIRECCION                  char(50)                       not null,
   primary key (ID_CLIENTE)
)
type = InnoDB;

/*==============================================================*/
/* Index: AGENDAR_FK                                            */
/*==============================================================*/
create index AGENDAR_FK on CLIENTE
(
   ID_CITA
);

/*==============================================================*/
/* Table: DETALLEFACTURA                                        */
/*==============================================================*/
create table DETALLEFACTURA
(
   ID_CLIENTE                     int                            not null,
   ID_CITA                        int                            not null,
   FAC_RUC                        char(13)                       not null,
   FAC_NUMERO                     char(9)                        not null,
   DETA_DESCRIPCION               char(50)                       not null,
   DETA_PVP                       decimal(4,2)                   not null,
   DETA_UNIDADES                  int                            not null,
   DETA_SUBTOTAL                  decimal(4,2)                   not null,
   primary key (ID_CLIENTE, ID_CITA)
)
type = InnoDB;

/*==============================================================*/
/* Index: DETALLEFACTURA2_FK                                    */
/*==============================================================*/
create index DETALLEFACTURA2_FK on DETALLEFACTURA
(
   ID_CLIENTE
);

/*==============================================================*/
/* Index: DETALLEFACTURA_FK                                     */
/*==============================================================*/
create index DETALLEFACTURA_FK on DETALLEFACTURA
(
   ID_CITA
);

/*==============================================================*/
/* Table: MASCOTAS                                              */
/*==============================================================*/
create table MASCOTAS
(
   ID_MASCOTA                     int                            not null,
   ID_CLIENTE                     int                            not null,
   MASC_NOMBRE                    char(20)                       not null,
   MASC_EDAD                      int                            not null,
   MASC_ESPECIE                   char(20)                       not null,
   MASC_RAZA                      char(20)                       not null,
   primary key (ID_MASCOTA)
)
type = InnoDB;

/*==============================================================*/
/* Index: TIENE_FK                                              */
/*==============================================================*/
create index TIENE_FK on MASCOTAS
(
   ID_CLIENTE
);

/*==============================================================*/
/* Table: RECETAS                                               */
/*==============================================================*/
create table RECETAS
(
   ID_RECETAS                     int                            not null,
   ID_CLIENTE                     int                            not null,
   ID_VETERINARIO                 int                            not null,
   REC_FECHA                      date                           not null,
   REC_PACIENTES                  char(30)                       not null,
   REC_DESCRIPCION                char(100)                      not null,
   REC_INDICACIONES               char(100)                      not null,
   primary key (ID_RECETAS)
)
type = InnoDB;

/*==============================================================*/
/* Index: ASIGNAR_FK                                            */
/*==============================================================*/
create index ASIGNAR_FK on RECETAS
(
   ID_CLIENTE
);

/*==============================================================*/
/* Index: REALIZA_FK                                            */
/*==============================================================*/
create index REALIZA_FK on RECETAS
(
   ID_VETERINARIO
);

/*==============================================================*/
/* Table: VETERINARIO                                           */
/*==============================================================*/
create table VETERINARIO
(
   ID_VETERINARIO                 int                            not null,
   VET_CEDULA                     char(13)                       not null,
   VET_NOMBRE                     char(30)                       not null,
   VET_APELLIDO                   char(30)                       not null,
   VET_ESPECIALIDAD               char(50),
   VET_TELEFONO                   varchar(13),
   VET_EMAIL                      varchar(20),
   primary key (ID_VETERINARIO)
)
type = InnoDB;

alter table CARNET add constraint FK_GENERAR foreign key (ID_VETERINARIO)
      references VETERINARIO (ID_VETERINARIO) on delete restrict on update restrict;

alter table CARNET add constraint FK_POSEER foreign key (ID_MASCOTA)
      references MASCOTAS (ID_MASCOTA) on delete restrict on update restrict;

alter table CITAS add constraint FK_AGENDAR2 foreign key (ID_CLIENTE)
      references CLIENTE (ID_CLIENTE) on delete restrict on update restrict;

alter table CITAS add constraint FK_ATENDER foreign key (ID_VETERINARIO)
      references VETERINARIO (ID_VETERINARIO) on delete restrict on update restrict;

alter table CLIENTE add constraint FK_AGENDAR foreign key (ID_CITA)
      references CITAS (ID_CITA) on delete restrict on update restrict;

alter table DETALLEFACTURA add constraint FK_DETALLEFACTURA foreign key (ID_CITA)
      references CITAS (ID_CITA) on delete restrict on update restrict;

alter table DETALLEFACTURA add constraint FK_DETALLEFACTURA2 foreign key (ID_CLIENTE)
      references CLIENTE (ID_CLIENTE) on delete restrict on update restrict;

alter table MASCOTAS add constraint FK_TIENE foreign key (ID_CLIENTE)
      references CLIENTE (ID_CLIENTE) on delete restrict on update restrict;

alter table RECETAS add constraint FK_ASIGNAR foreign key (ID_CLIENTE)
      references CLIENTE (ID_CLIENTE) on delete restrict on update restrict;

alter table RECETAS add constraint FK_REALIZA foreign key (ID_VETERINARIO)
      references VETERINARIO (ID_VETERINARIO) on delete restrict on update restrict;

